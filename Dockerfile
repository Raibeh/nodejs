FROM node:8-wheezy

##  APT
RUN apt-get update && \
    apt-get install -y mc && \
    npm install nodemon --global

##  NPM
WORKDIR /usr/src

COPY entrypoint.sh /usr/src/entrypoint.sh

ENTRYPOINT ./entrypoint.sh
#ENTRYPOINT sleep 1d
